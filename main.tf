# Configure the GitLab Provider
provider "gitlab" {
  token = var.gitlab_pat
}

# Add a project owned by the user
resource "gitlab_project" "new_project" {
  name        = var.new_project_name
  path        = var.new_project_path
  description = var.new_project_description

  default_branch = "main"

  lifecycle {
    prevent_destroy = true
  }
}

# Add a README
resource "gitlab_repository_file" "readme" {
  project        = gitlab_project.new_project.id
  file_path      = "README.md"
  branch         = "main"
  content        = base64encode("# ${var.new_project_name} 🚀")
  author_email   = var.ops_account_email
  author_name    = var.ops_account_name
  commit_message = "feature: add readme"
}
