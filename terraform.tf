terraform {
  required_version = ">= 1.4.5"
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.2.0"
    }
  }

  cloud {
    organization = "alexlab-cloud"

    workspaces {
      name = "gitlab"
    }
  }
}
