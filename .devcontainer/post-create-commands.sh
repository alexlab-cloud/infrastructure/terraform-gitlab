#!/bin/bash
# .devcontainer/post-create-commands.sh
#
# Extension script containing shell commands that are run once the container has been created.
# --------------------------------------------------------------------------------------------

# Provide permissions to the session to create and delete files
sudo chmod -R 777 "$CONTAINER_WORKSPACE_FOLDER"
